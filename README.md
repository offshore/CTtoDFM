# CTtoDFM

These scripts are part of the manuscript <b>Computed-tomography-based discrete fracture-matrix modelling: An integrated framework for deriving fracture networks</b>. This framework was tested and works on Windows 10.

Ferreira, C. A. S., Nick, H. M., 2023. Computed-tomography-based discrete fracture-matrix modelling: An integrated framework for deriving fracture networks. doi: [10.1016/j.advwatres.2023.104450](https://doi.org/10.1016/j.advwatres.2023.104450)

## Dependencies

The framework depends on and has been tested with:
- [Python 3](https://www.python.org/) (3.9.12);
- [ansys2meshio](https://gitlab.gbar.dtu.dk/offshore/ansys2meshio) (1.0.0)
- [meshio](https://github.com/nschloe/meshio) (5.3.4);
- [NumPy](https://numpy.org/) (1.21.6);
- [Open3D](http://www.open3d.org/docs/release/getting_started.html) (0.15.1);
- [OpenCV](https://pypi.org/project/opencv-python/) (4.5.5.64);
- [PyEVTK](https://github.com/paulo-herrera/PyEVTK) (1.5.0);
- [PyVista](https://www.pyvista.org/) (0.34.1);
- [scikit-image](https://scikit-image.org/) (0.19.2).

You also need [Rhinoceros 4.0](https://www.rhino3d.com/download/rhino/4.0) (SR9, license needed) for geometry generation, [Ansys ICEM CFD](https://www.ansys.com/products/meshing) (17.1, license needed) for meshing, and the [Docker Desktop](https://docs.docker.com/desktop/windows/install/) (4.7.1) manager due to dependencies limitations.

## Limitations

This library relies on [PyMesh](https://pymesh.readthedocs.io/en/latest/) for coarsening the mesh generated from the CT images of the fractures. Since PyMesh is not available for Windows, the coarsening routines are run inside a docker. Due to limitations of the Docker Desktop manager for Windows on mounting drives inside the docker container, these scripts can only run in the C drive. So you should clone this repository inside any directory in the C drive.

## Setting up Rhinoceros 4.0

This library relies on Rhino for generating a surface from a point cloud that represents the fractures in the sample. For that, we need to set up Rhino with the plug-in MeshFromPoints. In the command-line in Rhino, type PlugInManager, hit Enter, and it will open the window "Rhino Options". Click in the "Install" buttom, and select the file "MeshFromPoints.rhp". You may locate it in this repository, in the folder RhinoPlugIns, or download it [here](https://www.rhino3d.com/download/rhino/4.0/meshfrompoints). Hit Ok, and Rhino is set.

<p float="left">
	<img src="docs/figures/rhino_tutorial.png"/>
</p>

It is noteworth that Rhino does not perform an installation of the plug-in, but rather calls it. This means that if you move the "MeshFromPoints.rhp" file, Rhino will not find it and will return an error. You may want to copy the file to the plug-ins folder inside the folder where Rhinoceros 4.0 is installed. For example: "C:\Program Files (x86)\Rhinoceros 4.0\Plug-ins".

## Usage

The mixed-dimensional mesh is generated from a gray-scale-3D-image, where the fractures have pixel-value zero and the porous matrix have pixel-value 255. Currently, only the box-shaped model is supported. In future, we want to support also cilinder-shaped models. In case you do not have the segmented CT-image in a 3D-image, you may use the built-in crop and threshold functions.

### Parameters

First, set up the path to your Ansys and Rhinoceros 4.0 installations, and define the parameters for each model in the file "parameters.py":
```python
...
ansys_path = os.path.join('C:', '\Program Files', 'ANSYS Inc', 'v211')
rhino_path = os.path.join('C:', '\Program Files (x86)', 'Rhinoceros 4.0')
...
params['ValdemarS3'] = {'path_to_images': os.path.join('input', 'ValdemarS3'), 'path_to_meshes': os.path.join('output', 'ValdemarS3'), 'threshold_min': 5, 'threshold_max': 255, 'crop_list': [[350/1024, 610/1024], [350/1024, 590/1024], [200/1024, 850/1024]], 'cluster_eps': 16, 'min_samples': 8, 'coarsen_factor': 15e-2, 'min_elem_factor': 3e-2, 'pix2meter': 0.0417e-3}
...
```
The "params" dictionary contains the parameters for each model. The keys are the names of the samples, for calling the "main.py" script. The set-up values are:
- path_to_images: the directory containing the CT-scans (TIFF files), in case you want to use the built-in crop an threshold functions for generating the 3D-image;
- path_to_meshes: the directory that should contain the 3D-image, and also will store the geometry and mixed-dimensional mesh files;
- threshold_min: the minimum value for the built-in threshold function;
- threshold_max: the maximum value for the built-in threshold function;
- crop_list: a list containing the x, y and z points for which the sample will be croppped;
- cluster_eps: a distance value used for clustering the point cloud generated from the fractures, which is later used for reducing the noise in the skeletonized fracture;
- min_samples: minimum number of points (pixels) in a cluster;
- coarsen_factor: a factor for controlling the simplification of the fracture geometries;
- min_elem_factor: a factor for controlling the mesh refinement;
- pix2meter: a conversion factor from pixel to meters.

### Cropping

The tiff images are read and cropped into a 3D-image by calling
```bash
$ python main.py --sample 'ValdemarS3' --action 'crop CT scans'
```
In the "main.py" script, the output 3D-image is saved in a VTK file named "cropped.vti". This file is saved using [PyEVTK](https://github.com/paulo-herrera/PyEVTK). In addition, a TXT file containing the sizes (shape) of the box-shaped model is generated and saved with the default name "shape.txt".
```py
...
from ct2dfm.ct2pnts import CT2Pnts
ct2pnts = CT2Pnts(params[args.sample])
ct2pnts.read_ct_scans(0)
ct2pnts.crop()
ct2pnts.save_vtk(filename='cropped.vti')
ct2pnts.save_shape()
...
```

<i>Cropped imaged</i> (ParaView visualisation):
<p float="left">
	<img src="docs/figures/cropped.png"/>
</p>

### Thresholding

The 3D-image is thresholded by calling
```bash
$ python main.py --sample 'ValdemarS3' --action 'threshold'
```
In the "main.py" script, the output is saved in a VTK file named "segmented.vti".
```py
...
from ct2dfm.ct2pnts import CT2Pnts
ct2pnts = CT2Pnts(params[args.sample])
ct2pnts.read_shape()
ct2pnts.read_vtk(filename='cropped.vti')
ct2pnts.threshold()
ct2pnts.save_vtk(filename='segmented.vti')
...
```

<i>Thresholded image</i> (ParaView visualisation):
<p float="left">
	<img src="docs/figures/thresholded.png"/>
</p>

### Skeletonization

The segmented-3D-image is skeletonized by calling
```bash
$ python main.py --sample 'ValdemarS3' --action 'skeletonize'
```
In the "main.py" script, the output PLY file, with the point cloud (pixels) of the skeletonized fractures, is saved as "skeleton_cloud.ply". The point cloud is generated and saved using [Open3D](http://www.open3d.org/docs/release/getting_started.html).
```py
...
from ct2dfm.skeletonization import Skeletonizer
skel = Skeletonizer(params[args.sample])
skel.read_shape()
skel.read_vtk(filename='segmented.vti')
skel.skeletonize()
skel.save_ply(filename='skeleton_cloud.ply')
...
```
Following, the point cloud is clustered and the noise is removed, using a density-based spatial clustering of applications with noise (DBSCAN) algorithm, that is available in the Open3D library. The new point cloud is saved in a PLY file named "skeleton_dbscan.ply", so the user may check if the parameters cluster_eps and min_samples are well-adjusted. The list of points and apertures are saved in two TXT files, named "skeleton_points.txt" and "skeleton_point_data.txt", respectively.
```py
...
from ct2dfm.skeletonization import Clusterizer
clust = Clusterizer(params[args.sample])
clust.read_ply(filename='skeleton_cloud.ply')
clust.dbscan()
clust.save_ply(filename='skeleton_dbscan.ply')
clust.save_points(filename='skeleton_points.txt')
clust.save_point_data(filename='skeleton_point_data.txt')
...
```
Lastly, an STL file containing the fracture geometry is generated with Rhinoceros 4.0. For this step, a few remarks are important. First, the point cloud generated in the skeletonization step is imported to Rhino. Next, a marching-cubes algorithm available in Rhino is used to generate a mesh from this point cloud, which is saved as an STL file. In the "main.py" script, the STF file is named "skeleton_geometry.stl". Once Rhino finishes its calculations, it will automatically save the work in a 3DM file, close the project and open a new one. For the script to end, you have to manually close the Rhino window at this moment. The fracture geometry in the STL file is represented by a very fine mesh with characteristic length equal to 1 pixel, which should be later simplified with PyMesh.
```py
...
from ct2dfm.pnts2geom import Pnts2Geom
pnts2geom = Pnts2Geom(params[args.sample])
pnts2geom.read_shape()
pnts2geom.set_rhino_path(rhino_path)
pnts2geom.generate_fracture_geometry(input_file='skeleton_points.txt', output_file='skeleton_geometry.stl')
...
```

<i>Original skeleton surface, generated with Rhino</i> (ParaView visualisation):
<p float="left">
	<img src="docs/figures/original_surf.png"/>
</p>

### Geometry generation

The box-shaped model geometry is generated by calling
```bash
$ python main.py --sample 'ValdemarS3' --action 'generate geometry'
```
In this step, a Docker with [PyMesh](https://pymesh.readthedocs.io/en/latest/) is built and run, in which the "main.py" script will be called for simplifing the fracture geometry generated in the previous step. Furthermore, the geometry will be split into one file for each disconnected fracture. Finally, Rhino is again called to import the simplified fractures (STL files) and generating the geometry box-shaped model. Once done, the work is saved in a 3DM file and closed. When Rhino opens a new project, you should again manually close the window and the script will finish.
```py
...
from ct2dfm.pnts2geom import Pnts2Geom
pnts2geom = Pnts2Geom(params[args.sample])
pnts2geom.read_shape()
pnts2geom.set_rhino_path(rhino_path)
pnts2geom.simplify_fracture_geometry(args.sample)
pnts2geom.generate_model_geometry()
...
```

<i>Simplified skeleton surface, using PyMesh</i> (ParaView visualisation):
<p float="left">
	<img src="docs/figures/simplified_surf.png"/>
</p>

<i>Box-shaped geometry, generated with Rhino</i> (ParaView visualisation):
<p float="left">
	<img src="docs/figures/rhino_geometry.png"/>
</p>

### Mesh generation

Finally, the Rhino geometry is imported to [Ansys ICEM CFD](https://www.ansys.com/products/meshing), and an [Ansys Fluent](https://www.ansys.com/products/fluids/ansys-fluent) mesh composed of tetraedra is generated. 
```bash
$ python main.py --sample 'ValdemarS3' --action 'generate mesh'
```
This mesh can be easily converted with [ansys2meshio](https://gitlab.gbar.dtu.dk/offshore/ansys2meshio) and [meshio](https://github.com/nschloe/meshio) to any of the formats supported by meshio. In this example, we configured the script to convert the generated mesh to XDMF format. We have also configured it to convert to CSP format, through Ansys ICEM CFD.
```py
...
from ct2dfm.geom2mesh import Geom2Mesh
geom2mesh = Geom2Mesh(params[args.sample])
geom2mesh.read_shape()
geom2mesh.set_frac_list()
geom2mesh.set_ansys_path(ansys_path)
geom2mesh.convert_to_tin()
geom2mesh.generate_mesh()
del geom2mesh
from ct2dfm.geom2mesh import MeshFix
meshfix = MeshFix(params[args.sample])
meshfix.set_ansys_path(ansys_path)
meshfix.fix_boundary()
meshfix.fix_mesh_orientation()
meshfix.read_density(filename='cropped.vti')
meshfix.interpolate_density()
meshfix.calculate_porosity()
meshfix.populate_permeability()
meshfix.generate_fracture_mesh()
meshfix.read_skeleton_points(filename='skeleton_points.txt')
meshfix.read_skeleton_aperture(filename='skeleton_point_data.txt')
meshfix.interpolate_aperture()
meshfix.save_as(format='xdmf', save_frac=True)
meshfix.convert_to_csp()
```

<i>Mesh generated with ICEM CFD</i> (ParaView visualisation):
<p float="left">
	<img src="docs/figures/icem_mesh.png"/>
</p>