from .__init__ import *
import ansys2meshio
import json
import pyvista
from scipy.interpolate import NearestNDInterpolator, interp1d
from skspatial.objects import Plane, Triangle
import subprocess
from timethis import timethis


class Geom2Mesh(Foo2Pnts):
	def __init__(self, argsdict):
		super(Geom2Mesh, self).__init__(argsdict)

	def set_frac_list(self):
		with timethis('Setting fracture list'):
			self.frac_files = [f for f in os.listdir(self.path_to_meshes) if os.path.isfile(os.path.join(self.path_to_meshes, f)) and 'INT_FRAC' in f and '.stl' in f]
			self.frac_names = [f.replace('.stl', '') for f in self.frac_files]

	def set_ansys_path(self, path):
		self.ansys_path = path

	def convert_to_tin(self):
		print('Converting 3DM to TIN...')
		with timethis('Converting 3DM to TIN'):
			cnvrtr_path = os.path.join(self.ansys_path, 'icemcfd', 'win64_amd', 'bin', '3dm2tin.exe')
			subprocess.call([cnvrtr_path, '-a', os.path.join(os.getcwd(), self.path_to_meshes, 'output.3dm')])

	def generate_mesh(self, max_factor=2e-1):
		print('Generating mesh...')
		with timethis('Generating mesh'):
			with timethis('Generating ANSYS ICEM script'):
				gmax = np.min(self._shape)*max_factor
				gnat = np.min(self._shape)*self.min_elem_factor
				outfilename = os.path.join(os.getcwd(), self.path_to_meshes, 'output')
				fname = os.path.split(self.path_to_meshes)[-1]
				tmpfilename = os.path.join(os.getcwd(), '.cache', 'tetra_{}'.format(fname))
				cnvrtr_path = os.path.join(self.ansys_path, 'icemcfd', 'win64_amd', 'icemcfd', 'output-interfaces', 'fluent6')
				icem_script = ''
				icem_script += 'ic_load_tetin {{{}}}\n'.format(outfilename + '.tin')
				icem_script += 'ic_boco_solver\n'
				icem_script += 'ic_boco_clear_icons\n'
				icem_script += 'ic_geo_new_family MATRIX\n'
				icem_script += 'ic_boco_set_part_color MATRIX\n'
				for _ in range(50*len(self.frac_names)):
					icem_script += 'ic_geo_create_volume {{{} {} {}}} {{}} MATRIX\n'.format(*(np.random.sample(3)*self._shape))
				icem_script += 'ic_set_global geo_cad 0.4 toler\n'
				icem_script += 'ic_set_global geo_cad 1 toptol_userset\n'
				icem_script += 'ic_geo_delete_unattached { ' + ' '.join(self.frac_names) + ' RIGHT FRONT BACK LEFT TOP BOTTOM ORFN} 0 1\n'
				icem_script += 'ic_build_topo 1 -angle 30 -no_concat ' + ' '.join(self.frac_names) + ' RIGHT FRONT BACK LEFT TOP BOTTOM ORFN\n'
				icem_script += 'ic_geo_delete_unattached { ' + ' '.join(self.frac_names) + ' RIGHT FRONT BACK LEFT TOP BOTTOM ORFN}\n'
				for frac in self.frac_names:
					icem_script += 'ic_geo_set_family_params {} no_crv_inf prism 0 emax 0.0 ehgt 0 hrat 0 nlay 0 erat 0 ewid 0 emin 0.0 edev 0.0 prism_height_limit 0 law -1 split_wall 0 internal_wall 1\n'.format(frac)
				icem_script += 'ic_set_meshing_params global 0 gref 1.0 gmax {} gfast 0 gedgec 0.2 gnat {} gcgap 2 gnatref 10 igwall 1\n'.format(gmax, gnat)
				icem_script += 'ic_set_meshing_params variable 0 tetra_smooth 1\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_smooth_aspect 0.5\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_smooth_niter 10\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_fix_nmanifold 1\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_coarsen 1\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_coarsen_aspect 0.3\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_coarsen_niter 10\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_fix_holes 1\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_close_gaps 1\n'
				icem_script += 'ic_set_meshing_params variable 0 tetra_fix_nmanifold 1\n'
				icem_script += 'ic_set_meshing_params global 0 gfast 0 gedgec 0.2\n'
				icem_script += 'ic_save_tetin {{{}}}\n'.format(tmpfilename + '.tin')
				icem_script += 'ic_run_tetra {{{}}} {{{}}} run_cutter 1 delete_auto 1 closegaps 1 run_smoother 0 fix_holes 1 n_processors 1 in_process 1 log {{{}}}\n'.format(tmpfilename + '.tin', outfilename + '.uns', outfilename + '.log')
				icem_script += 'ic_geo_set_modified 1\n'
				icem_script += 'ic_uns_update_family_type visible {' + ' '.join(self.frac_names) + ' RIGHT FRONT BACK LEFT TOP BOTTOM ORFN SHEET} {!NODE !LINE_2 TRI_3 !TETRA_4} update 0\n'
				icem_script += 'ic_boco_solver\n'
				icem_script += 'ic_boco_clear_icons\n'
				icem_script += 'ic_uns_subset_visible {added faces} 1\n'
				icem_script += 'ic_uns_coarsen all 0.2 {{}} 0.1 5 1 0 {}\n'.format(gmax)
				icem_script += 'ic_uns_diagnostic diag_type single quiet 1\n'
				icem_script += 'ic_smooth_elements map all upto 0.5 iterations 10 fix_families {} n_processors 1 smooth TRI_3 float TETRA_4 laplace 1\n'
				icem_script += 'ic_smooth_elements map all upto 0.5 iterations 10 prism_warp_weight 0.5 fix_families {} n_processors 1 smooth TETRA_4 float PENTA_6 freeze TRI_3\n'
				icem_script += 'ic_smooth_elements map all upto 0.5 iterations 10 prism_warp_weight 0.5 fix_families {} metric Quality n_processors 1 smooth TETRA_4 smooth TRI_3 float PENTA_6\n'
				icem_script += 'ic_geo_set_modified 1\n'
				icem_script += 'ic_delete_empty_parts\n'
				icem_script += 'ic_uns_check_duplicate_numbers\n'
				icem_script += 'ic_uns_renumber_all_elements 1 1\n'
				icem_script += 'ic_uns_build_mesh_topo all -angle 150 -uncovered_element_family UNCOVERED -create_bars_family_intersection\n'
				icem_script += 'ic_uns_quad_remesh all element 3 how 7 proj 0 all_dim 0 max_steps 100 quality 0.8 diagnostic Quality\n'
				for _ in range(5):
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type duplicate fix_fam FIX_DUPLICATE disp_subset uns_diag_0 diag_verb {Duplicate elements} fams {} busy_off 1\n'
					icem_script += 'ic_uns_delete_contents uns_diag_0\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type missing_internal fix_fam FIX_MISSING_INTERNAL disp_subset uns_diag_0 diag_verb {Missing internal faces} fams {} busy_off 1\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type vol_orient fix_fam FIX_VOL_ORIENT disp_subset uns_diag_0 diag_verb {Volume orientations} fams {} busy_off 1\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type surf_orient fix_fam FIX_SURF_ORIENT disp_subset uns_diag_0 diag_verb {Surface orientations} fams {} busy_off 1\n'
					icem_script += 'ic_uns_subset_configure uns_diag_0 -width 0\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type hanging fix_fam FIX_HANGING disp_subset uns_diag_0 diag_verb {Hanging elements} fams {} busy_off 1\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type penetrating fix_fam FIX_PENETRATING disp_subset uns_diag_0 diag_verb {Penetrating elements} fams {} busy_off 1\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type disconnbars fix_fam FIX_DISCONNBARS disp_subset uns_diag_0 diag_verb {Disconnected bar elements} fams {} busy_off 1\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_make_consistent all {} 0\n'
					icem_script += 'ic_flood_fill no_repair\n'
					icem_script += 'ic_uns_subset_create LeakLocation\n'
					icem_script += 'ic_uns_subset_clear LeakLocation\n'
					icem_script += 'ic_uns_diagnostic diag_type single disp_subset LeakLocation quiet 1\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type nmanvert fix_fam FIX_NMANVERT disp_subset uns_diag_0 diag_verb {Non-manifold vertices} fams {} busy_off 1\n'
					icem_script += 'ic_uns_fix_non_man uns_diag_0\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type single_2 fix_fam FIX_SINGLE_2 disp_subset uns_diag_0 diag_verb {2-Single edges} fams {} busy_off 1\n'
					icem_script += 'ic_uns_delete_contents uns_diag_0\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type single_multiple fix_fam FIX_SINGLE_MULTIPLE disp_subset uns_diag_0 diag_verb {Single-Multiple edges} fams {} busy_off 1\n'
					icem_script += 'ic_uns_delete_contents uns_diag_0\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type standalone fix_fam FIX_STANDALONE disp_subset uns_diag_0 diag_verb {Stand-alone surface mesh} fams {} busy_off 1\n'
					icem_script += 'ic_uns_delete_contents uns_diag_0\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
				for _ in range(10):
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type uncovered fix_fam FIX_UNCOVERED disp_subset uns_diag_0 diag_verb {Uncovered faces} fams {} busy_off 1\n'
					icem_script += 'ic_uns_fix_uncovered_faces uns_diag_0 MATRIX\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
					icem_script += 'ic_uns_create_diagnostic_subset\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 1\n'
					icem_script += 'ic_uns_diagnostic subset all diag_type triangle_box fix_fam FIX_TRIANGLE_BOX disp_subset uns_diag_0 diag_verb {Triangle boxes} fams {} busy_off 1\n'
					icem_script += 'ic_uns_fix_triangle_boxes uns_diag_0 MATRIX\n'
					icem_script += 'ic_uns_subset_delete uns_diag_0\n'
					icem_script += 'ic_uns_create_diagnostic_edgelist 0\n'
				icem_script += 'ic_save_unstruct {{{}}} 1 {{}} {{}} {{}}\n'.format(tmpfilename + '.uns')
				icem_script += 'ic_boco_solver {ANSYS Fluent}\n'
				icem_script += 'ic_solution_set_solver {ANSYS Fluent} 1\n'
				icem_script += 'ic_boco_save {{{}}}\n'.format(tmpfilename + '.fbc')
				icem_script += 'ic_exec {{{}}} -dom {{{}}} -b {{{}}} {{{}}}\n'.format(cnvrtr_path, tmpfilename + '.uns', tmpfilename + '.fbc', outfilename)
				script_path = os.path.join(os.getcwd(), '.cache', 'geom2mesh_{}.rpl'.format(fname))
				f = open(script_path, 'w')
				f.write(icem_script.replace('\\', '/'))
				f.close()
			with timethis('Running ANSYS ICEM'):
				icem_path = os.path.join(self.ansys_path, 'icemcfd', 'win64_amd', 'bin', 'icemcfd.bat')
				subprocess.call([icem_path, '-batch', '-script', script_path])


class MeshFix(Foo2Pnts):
	def __init__(self, argsdict):
		super(MeshFix, self).__init__(argsdict)
		with timethis('Reading ANSYS fluent mesh'):
			self.cells, self.zones = ansys2meshio.read(os.path.join(os.getcwd(), self.path_to_meshes, 'output.msh'), return_zones=True)

	def get_zones(self):
		zones = self.zones.copy()
		for key in self.zones.copy().keys():
			n, _ = self.zones[key]
			zones.pop(key)
			zones[n] = key
		return zones

	def fix_dimensions(self):
		with timethis('Fixing dimensions'):
			self.cells.points *= self.pix2meter

	def fix_boundary(self):
		print('Fixing boundaries...')
		with timethis('Fixing boundaries'):
			zones = self.get_zones()
			points = self.cells.points.copy()
			shape = points.max(axis=0)
			box = [[zones['LEFT'], zones['RIGHT']], [zones['FRONT'], zones['BACK']], [zones['BOTTOM'], zones['TOP']]]
			bound_dict = {}
			for i in range(3):
				for j in range(2):
					bound_dict[box[i][j]] = (i, shape[i]*j)
			facets = self.cells.get_cells_type('triangle')
			boundaries = self.cells.get_cell_data('meshtags', 'triangle')
			for k, tag in enumerate(boundaries):
				if tag in bound_dict.keys():
					for p in facets[k]:
						points[p, bound_dict[tag][0]] = bound_dict[tag][1]
			self.cells.points = points.copy()
			cells = self.cells.get_cells_type('tetra')
			cell_data = self.cells.get_cell_data('meshtags', 'tetra')
			filter = np.ones(len(cells), dtype=bool)
			for k, cell in enumerate(cells):
				p0 = points[cell[0]]
				p1 = points[cell[1]]
				p2 = points[cell[2]]
				p3 = points[cell[3]]
				volume = abs(np.dot(p1 - p0, np.cross(p2 - p0, p3 - p0))/6)
				filter[k] = volume > 0
			self.cells.cells[0].data = cells[filter]
			self.cells.cell_data['meshtags'][0] = cell_data[filter]
			for k, tag in enumerate(boundaries):
				if not tag in bound_dict.keys():
					for i in range(3):
						for j in range(2):
							if np.all(points[facets[k]][:, i] == shape[i]*j):
								self.cells.cell_data['meshtags'][1][k] = box[i][j]
			self.fix_dimensions()

	def fix_mesh_orientation(self):
		with timethis('Fixing normal orientations'):
			print('Fixing normal orientations')
			facets = self.cells.get_cells_type('triangle')
			count = 0
			for i, facet in enumerate(facets):
				p0 = self.cells.points[facet[0]]
				p1 = self.cells.points[facet[1]]
				p2 = self.cells.points[facet[2]]
				triangle = Triangle(p0, p1, p2)
				if triangle.is_right():
					count += 1
					self.cells.cells_dict['triangle'][i][1] = facet[2]
					self.cells.cells_dict['triangle'][i][2] = facet[1]				
			print('| Fixed the orientation of {} triangles'.format(count))

	def read_density(self, filename):
		with timethis('Reading density from VTK file'):
			print('Reading density from VTK file')
			with timethis('Reading with PyVista'):
				path_to_file = os.path.join(self.path_to_meshes, filename)
				vtk_file = pyvista.read(path_to_file)
				points = vtk_file.points*self.pix2meter
				data = vtk_file['density']
			with timethis('Setting NN-interpolator'):
				self.get_density = NearestNDInterpolator(list(zip(points[:, 0], points[:, 1], points[:, 2])), data)

	def interpolate_density(self):
		with timethis('Interpolating density'):
			self.cells.cell_data['density'] = []
			density_data = []
			barycentric_coordinates = np.array([[0.2500000000000000, 0.2500000000000000, 0.2500000000000000, 0.2500000000000000], [0.8160578438945539, 0.0919710780527230, 0.0919710780527230, 0.0919710780527230], [0.0919710780527230, 0.8160578438945539, 0.0919710780527230, 0.0919710780527230], [0.0919710780527230, 0.0919710780527230, 0.8160578438945539, 0.0919710780527230], [0.0919710780527230, 0.0919710780527230, 0.0919710780527230, 0.8160578438945539], [0.3604127443407402, 0.3197936278296299, 0.3197936278296299, 0.3197936278296299], [0.3197936278296299, 0.3604127443407402, 0.3197936278296299, 0.3197936278296299], [0.3197936278296299, 0.3197936278296299, 0.3604127443407402, 0.3197936278296299], [0.3197936278296299, 0.3197936278296299, 0.3197936278296299, 0.3604127443407402], [0.1381966011250105, 0.1381966011250105, 0.3618033988749895, 0.3618033988749895], [0.1381966011250105, 0.3618033988749895, 0.1381966011250105, 0.3618033988749895], [0.1381966011250105, 0.3618033988749895, 0.3618033988749895, 0.1381966011250105], [0.3618033988749895, 0.1381966011250105, 0.1381966011250105, 0.3618033988749895], [0.3618033988749895, 0.1381966011250105, 0.3618033988749895, 0.1381966011250105], [0.3618033988749895, 0.3618033988749895, 0.1381966011250105, 0.1381966011250105]])
			for cell in self.cells.cells_dict['tetra']:
				p0 = self.cells.points[cell[0]]
				p1 = self.cells.points[cell[1]]
				p2 = self.cells.points[cell[2]]
				p3 = self.cells.points[cell[3]]
				volume = abs(np.dot(p1 - p0, np.cross(p2 - p0, p3 - p0))/6)
				vertices = np.array([p0, p1, p2, p3])
				quadrature_points = []
				for barycentric_coordinate in barycentric_coordinates:
					node_Q = np.sum((vertices*np.array([barycentric_coordinate]).T), axis=0)
					quadrature_points.append(node_Q)
				quadrature_points = np.array(quadrature_points)
				quadrature_weights = np.array([[0.1185185185185185*volume], [0.0719370837790186*volume], [0.0719370837790186*volume], [0.0719370837790186*volume], [0.0719370837790186*volume], [0.0690682072262724*volume], [0.0690682072262724*volume], [0.0690682072262724*volume], [0.0690682072262724*volume], [0.0529100529100529*volume], [0.0529100529100529*volume], [0.0529100529100529*volume], [0.0529100529100529*volume], [0.0529100529100529*volume], [0.0529100529100529*volume]])
				density = 0
				for p, w in zip(quadrature_points, quadrature_weights):
					density += self.get_density(p)*w
				density_data.append(density/volume)
			self.cells.cell_data['density'].append(np.array(density_data).reshape(-1))
			density_data = []
			barycentric_coordinates = np.array([[0.3333333333333333, 0.3333333333333333, 0.3333333333333333], [0.7974269853530870, 0.1012865073234560, 0.1012865073234560], [0.1012865073234560, 0.7974269853530870, 0.1012865073234560], [0.1012865073234560, 0.1012865073234560, 0.7974269853530870], [0.0597158717897700, 0.4701420641051150, 0.4701420641051150], [0.4701420641051150, 0.0597158717897700, 0.4701420641051150], [0.4701420641051150, 0.4701420641051150, 0.0597158717897700]])
			for cell in self.cells.cells_dict['triangle']:
				p0 = self.cells.points[cell[0]]
				p1 = self.cells.points[cell[1]]
				p2 = self.cells.points[cell[2]]
				triangle = Triangle(p0, p1, p2)
				area = triangle.area()
				vertices = np.array([p0, p1, p2])
				quadrature_points = []
				for barycentric_coordinate in barycentric_coordinates:
					node_Q = np.sum((vertices*np.array([barycentric_coordinate]).T), axis=0)
					quadrature_points.append(node_Q)
				quadrature_points = np.array(quadrature_points)
				quadrature_weights = np.array([[0.225000000000000*area], [0.125939180544827*area], [0.125939180544827*area], [0.125939180544827*area], [0.132394152788506*area], [0.132394152788506*area], [0.132394152788506*area]])
				density = 0
				for p, w in zip(quadrature_points, quadrature_weights):
					density += self.get_density(p)*w
				density_data.append(density/area)
			self.cells.cell_data['density'].append(np.array(density_data).reshape(-1))

	def calculate_porosity(self):
		with timethis('Calculating porosity'):
			print('Calculating porosity')
			with timethis('Setting 1D-interpolator'):
				porosity = interp1d(x=[255., self.ref_dens, 0.], y=[0., self.ref_pore, 1.], kind='quadratic')
			with timethis('Interpolating porosity'):
				self.cells.cell_data['porosity'] = []
				for density_data in self.cells.cell_data['density']:
					porosity_data = np.array([porosity(density) for density in density_data]).reshape(-1)
					self.cells.cell_data['porosity'].append(porosity_data)

	def populate_permeability(self):
		with timethis('Populating permeability'):
			permeability = lambda porosity: (0.52*(0.05*np.exp(0.08*porosity))**1.083)*9.869233e-16 # Correlation for Lower Tuxen by Glad et al (https://doi.org/10.1016/j.marpetgeo.2021.105445), corrected for chalk using correlation by Mortensen et al (https://doi.org/10.2118/31062-PA)
			self.cells.cell_data['permeability'] = []
			for porosity_data in self.cells.cell_data['porosity']:
				permeability_data = np.array([permeability(porosity) for porosity in porosity_data]).reshape(-1)
				self.cells.cell_data['permeability'].append(permeability_data)
			
	def generate_fracture_mesh(self):
		with timethis('Generating fracture mesh'):
			zones = self.get_zones()
			frac_cells = np.empty((0, 3), dtype=int)
			meshtags = []
			for key in zones.copy().keys():
				if 'INT_FRAC' in key:
					frac_cells = np.vstack([frac_cells, self.cells.get_cells_type('triangle')[np.where(self.cells.get_cell_data('meshtags', 'triangle').reshape(-1) == zones[key])]])
					meshtags += (self.cells.get_cell_data('meshtags', 'triangle').reshape(-1)[np.where(self.cells.get_cell_data('meshtags', 'triangle').reshape(-1) == zones[key])]).tolist()
			frac_points = self.cells.points[np.unique(frac_cells.flatten())]
			pntsmap = np.unique(frac_cells.flatten())
			for i in range(len(frac_cells)):
				for j in range(3):
					p = frac_cells[i, j]
					frac_cells[i, j] = np.where(pntsmap == p)[0][0]
			self.frac = ansys2meshio.Mesh(points=frac_points, cells={'triangle': frac_cells}, cell_data={'meshtags': [meshtags]})

	def read_skeleton_points(self, filename):
		with timethis('Reading skeleton points'):
			self.points = np.loadtxt(os.path.join(self.path_to_meshes, filename), delimiter=',')
			self.points *= self.pix2meter
	
	def read_skeleton_aperture(self, filename):
		with timethis('Reading skeleton aperture'):
			self.aperture = np.loadtxt(os.path.join(self.path_to_meshes, filename), delimiter=',').reshape((-1, 1))
			self.aperture *= self.pix2meter

	def interpolate_aperture(self, eps=1e-3):
		with timethis('Interpolating aperture/permeability'):
			points = self.points.copy()
			aperture = self.aperture.copy()
			aperture_data = []
			for cell in self.frac.cells_dict['triangle']:
				p0 = self.frac.points[cell[0]]
				p1 = self.frac.points[cell[1]]
				p2 = self.frac.points[cell[2]]
				n = np.cross(p1 - p0, p2 - p0)
				n /= np.linalg.norm(n)
				triangle = Triangle(p0, p1, p2)
				dist = np.sqrt(((points - triangle.centroid())**2).sum(axis=1))
				area = triangle.area()
				semiperimeter = triangle.perimeter()/2
				inradius = area/semiperimeter
				circumradius = triangle.length('a')*triangle.length('b')*triangle.length('c')/(4*inradius*semiperimeter)
				filter = dist < circumradius
				points_maybe = points[np.where(filter)]
				aperture_maybe = aperture[np.where(filter)]
				plane = Plane(point=p0, normal=n)
				j = 0
				a = np.zeros((np.max([np.floor(area/self.pix2meter**2).astype(int), 1]), 2))
				a[:, 0] = 1e40
				a[:, 1] = self.pix2meter/10
				while j < len(points_maybe):
					dist = plane.distance_point(points_maybe[j])
					if dist < (2 + eps)*self.pix2meter:
						p = plane.project_point(points_maybe[j])
						pa = triangle.point('A') - p
						pb = triangle.point('B') - p
						pc = triangle.point('C') - p
						alpha = np.linalg.norm(np.cross(pb, pc))/(2*area)
						beta = np.linalg.norm(np.cross(pc, pa))/(2*area)
						gamma = 1 - alpha - beta
						if (alpha < 1 + eps and alpha > -eps and beta < 1 + eps and beta > -eps and gamma < 1 + eps and gamma > -eps):
							a[-1, 0] = dist
							a[-1, 1] = aperture_maybe[j]
							a = a[a[:, 0].argsort()]
							points_maybe = np.delete(points_maybe, j, axis=0)
							aperture_maybe = np.delete(aperture_maybe, j, axis=0)
						else:
							j += 1
					else:
						j += 1
				aperture_data.append(np.mean(a[:, 1]**3)**(1/3))
			aperture_data = np.array(aperture_data)
			self.frac = ansys2meshio.Mesh(points=self.frac.points, cells={'triangle': self.frac.get_cells_type('triangle')}, cell_data={'meshtags': [self.frac.get_cell_data('meshtags', 'triangle')], 'aperture': [aperture_data], 'permeability': [aperture_data**2/12]})
			self.cells.cell_data['aperture'] = []
			for i, block in enumerate(self.cells.cells):
				self.cells.cell_data['aperture'].append(np.zeros(len(self.cells.get_cells_type(block.type))))
				if block.type == 'triangle':
					for meshtag in set(self.cells.get_cell_data('meshtags', 'triangle')):
						if meshtag in self.frac.get_cell_data('meshtags', 'triangle'):
							self.cells.cell_data['aperture'][i][np.where(meshtag == self.frac.get_cell_data('meshtags', 'triangle'))] = self.frac.get_cell_data('aperture', 'triangle')[np.where(meshtag == self.frac.get_cell_data('meshtags', 'triangle'))]
							self.cells.cell_data['permeability'][i][np.where(meshtag == self.frac.get_cell_data('meshtags', 'triangle'))] = self.frac.get_cell_data('permeability', 'triangle')[np.where(meshtag == self.frac.get_cell_data('meshtags', 'triangle'))]

	def save_as(self, format='xdmf', save_frac=False):
		with timethis('Saving mesh files'):
			matrixname = os.path.join(os.getcwd(), self.path_to_meshes, 'matrix.')
			matrixmesh = ansys2meshio.Mesh(points=self.cells.points, cells={'tetra': self.cells.get_cells_type('tetra')}, cell_data={key: [self.cells.get_cell_data(key, 'tetra')] for key in self.cells.cell_data.keys()})
			matrixmesh.write(matrixname + format)
			regionsname = os.path.join(os.getcwd(), self.path_to_meshes, 'regions')
			f = open(regionsname + '.json', 'w')
			zones = self.get_zones()
			json.dump(zones, f, indent=5)
			f.close()
			if save_frac:
				fracname = os.path.join(os.getcwd(), self.path_to_meshes, 'fractures.')
				self.frac.write(fracname + format)

	def set_ansys_path(self, path):
		self.ansys_path = path

	def convert_to_csp(self):
		print('Converting to CSP format...')
		with timethis('Converting to CSP format'):
			with timethis('Saving ANSYS Fluent mesh'):
				self.cells.write(os.path.join(os.getcwd(), self.path_to_meshes, 'output_fix.msh'), zones=self.zones)
			with timethis('Generating ANSYS ICEM script'):
				fname = os.path.split(self.path_to_meshes)[-1]
				outfilename = os.path.join(os.getcwd(), self.path_to_meshes, 'output_fix')
				tmpfilename = os.path.join(os.getcwd(), '.cache', 'tetra_{}'.format(fname))
				reader_path = os.path.join(self.ansys_path, 'icemcfd', 'win64_amd', 'icemcfd', 'result-interfaces', 'readfluent.exe')
				cnvrtr_path = os.path.join(self.ansys_path, 'icemcfd', 'win64_amd', 'icemcfd', 'output-interfaces', 'csp')
				icem_script = ''
				icem_script += 'ic_read_external {{{}}} {{{}}} 1 2 0 {{}}\n'.format(reader_path, outfilename + '.msh')
				icem_script += 'ic_save_unstruct {{{}}} 1 {{}} {{}} {{}}\n'.format(tmpfilename + '.uns')
				icem_script += 'ic_boco_solver {CSP}\n'
				icem_script += 'ic_solution_set_solver {CSP} 1\n'
				icem_script += 'ic_solver_mesh_info {CSP}\n'
				icem_script += 'ic_boco_save {{{}}}\n'.format(tmpfilename + '.fbc')
				icem_script += 'ic_exec {{{}}} -dom {{{}}} -b {{{}}} {{{}}}\n'.format(cnvrtr_path, tmpfilename + '.uns', tmpfilename + '.fbc', outfilename)
				script_path = os.path.join(os.getcwd(), '.cache', 'ansys2csp_{}.rpl'.format(fname))
				f = open(script_path, 'w')
				f.write(icem_script.replace('\\', '/'))
				f.close()
			with timethis('Running ANSYS ICEM'):
				icem_path = os.path.join(self.ansys_path, 'icemcfd', 'win64_amd', 'bin', 'icemcfd.bat')
				subprocess.call([icem_path, '-batch', '-script', script_path])
			with timethis('Saving regions'):
				regionsfilename = os.path.join(os.getcwd(), self.path_to_meshes, 'output _fix-regions')
				f = open("{}.txt".format(regionsfilename), 'w')
				f.write("'{}.txt' file\n".format(regionsfilename))
				f.write("no properties\n")
				for key in self.zones.keys():
					f.write("{}\n".format(self.zones[key][0]))
				f.close()
		with timethis('Exporting CT-data'):
			file1 = open(os.path.join(os.getcwd(), self.path_to_meshes, 'output_fix-porosity.txt'), 'w')
			file2 = open(os.path.join(os.getcwd(), self.path_to_meshes, 'output_fix-thickness.txt'), 'w')
			for block in self.cells.cells:
				if block.type == 'tetra':
					for c, cell in enumerate(block.data):
						points = np.array([self.cells.points[p] for p in cell])
						centroid = points.mean(axis=0)
						data = np.concatenate([centroid, [self.cells.get_cell_data('porosity', block.type)[c]]])
						file1.write(','.join(data.astype(str)) + '\n')
			for block in self.frac.cells:
				if block.type == 'triangle':
					for c, cell in enumerate(block.data):
						points = np.array([self.frac.points[p] for p in cell])
						centroid = points.mean(axis=0)
						data = np.concatenate([centroid, [self.frac.get_cell_data('aperture', block.type)[c]]])
						file2.write(','.join(data.astype(str)) + '\n')
			file1.close()
			file2.close()