from .ct2pnts import *
import skimage
from timethis import timethis


class Skeletonizer(CT2Pnts):
	def __init__(self, argsdict):
		super(Skeletonizer, self).__init__(argsdict)

	def skeletonize(self):
		print('Skeletonizing...')
		with timethis('Skeletonizing'):
			with timethis('Inverting image'):
				skel_img = np.zeros(self._img.shape)
				thick_img = np.zeros(self._img.shape)
				bin_img = skimage.util.invert(self._img/255)
			for i in range(3):
				with timethis('Skeletonizing, step {}'.format(i + 1)):
					for j in range(self._img.shape[i]):
						_, thickness = skimage.morphology.medial_axis(np.moveaxis(bin_img, i, 0)[j], return_distance=True)
						skeleton = skimage.morphology.skeletonize(np.moveaxis(bin_img, i, 0)[j], method='lee')
						curr_slice = np.moveaxis(skel_img, i, 0)[j]
						if curr_slice.sum() < skeleton.sum():
							np.moveaxis(skel_img, i, 0)[j] = skeleton
							np.moveaxis(thick_img, i, 0)[j] = thickness
			with timethis('Setting points'):
				points = np.vstack(np.where(skel_img != 0)).T
			with timethis('Setting point data'):
				thickness = np.array([thick_img[p[0], p[1], p[2]] for p in points])
			with timethis('Setting point cloud'):
				self._pcd = o3d.geometry.PointCloud()
				self._pcd.points = o3d.utility.Vector3dVector(points)
				colors = np.zeros(points.shape)
				colors[:, 0] = thickness/255
				self._pcd.points = o3d.utility.Vector3dVector(points)
				self._pcd.colors = o3d.utility.Vector3dVector(colors)
				self._points = points
				self._aperture = thickness


class Clusterizer(CT2Pnts):
	def __init__(self, argsdict):
		super(Clusterizer, self).__init__(argsdict)

	def dbscan(self):
		print('Clustering via DBSCAN...')
		with timethis('Clustering via DBSCAN'):
			with timethis('Clustering'):
				labels = np.asarray(self._pcd.cluster_dbscan(eps=self.cluster_eps, min_points=self.min_samples))
			with timethis('Setting point cloud'):
				points = np.asarray(self._pcd.points)[labels != -1]
				thickness = np.asarray(self._pcd.colors)[:, 0][labels != -1]*255
				colors = np.zeros(points.shape)
				colors[:, 0] = thickness/255
				self._pcd.points = o3d.utility.Vector3dVector(points)
				self._pcd.colors = o3d.utility.Vector3dVector(colors)
				self._points = points
				self._aperture = thickness