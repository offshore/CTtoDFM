from .__init__ import *
import cv2
import evtk
import open3d as o3d
from timethis import timethis
import pyvista


class CT2Pnts(Foo2Pnts):
	def __init__(self, argsdict):
		super(CT2Pnts, self).__init__(argsdict)

	def read_ct_scans(self, flag=-1):
		print('Reading CT scans...')
		with timethis('Reading CT scans'):
			with timethis('Collecting TIF files'):
				files = sorted([f for f in os.listdir(self.path_to_images) if os.path.isfile(os.path.join(self.path_to_images, f)) and ('.tif' in f or '.TIF' in f)])
			with timethis('Reading TIF files'):
				def tif_generator():
					for f in files:
						path_to_image = os.path.join(self.path_to_images, f)
						img = cv2.imread(path_to_image, flag)
						yield img
				self._img = np.stack(tif_generator())
				self._img = np.moveaxis(self._img, [0, 2], [2, 0])
				self._img = np.moveaxis(self._img, [0, 1], [1, 0])
				self._shape = self._img.shape

	def crop(self):
		dimensions = self.get_shape()
		with timethis('Cropping 3D image'):
			for dim, limits in enumerate(self.crop_list):
				_init = int(dimensions[dim]*limits[0])
				_end = int(dimensions[dim]*limits[1])
				if dim == 0:
					self._img = self._img[_init:_end, ...]
				elif dim == 1:
					self._img = self._img[:, _init:_end, ...]
				elif dim == 2:
					self._img = self._img[..., _init:_end]
			self._shape = self._img.shape

	def threshold(self):
		with timethis('Segmenting 3D image'):
			block_size = round(np.array(self.get_shape())[:1].mean()/4)
			block_size += 1 if block_size%2 == 0 else 0
			for i in range(self.get_shape()[2]):
				_, self._img[:, :, i] = cv2.threshold(self._img[:, :, i], np.median(self._img[:, :, i]), 255, cv2.THRESH_TRUNC)
				self._img[:, :, i] = cv2.adaptiveThreshold(src=self._img[:, :, i].astype(np.uint8), adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C, thresholdType=cv2.THRESH_BINARY, maxValue=self.threshold_max, blockSize=block_size, C=self.threshold_min)/self.threshold_max*255
			with open(os.path.join(self.path_to_meshes, 'threshold_parameters.txt'), 'w') as f:
				f.write('threshold_min: {}\n'.format(self.threshold_min))
				f.write('threshold_max: {}\n'.format(self.threshold_max))
				f.close()

	def read_vtk(self, filename):
		with timethis('Reading VTK file'):
			with timethis('Reading with PyVista'):
				path_to_file = os.path.join(self.path_to_meshes, filename)
				vtk_file = pyvista.read(path_to_file)
			with timethis('Setting 3D image'):
				coords = vtk_file.points.astype(int)
				shape = tuple(vtk_file.dimensions)
				self._img = np.zeros(shape)
				data = vtk_file['density']
				self._img[coords[:, 0], coords[:, 1], coords[:, 2]] = data
				self._shape = self._img.shape
				
	def save_vtk(self, filename='output.vti'):
		with timethis('Saving VTK file'):
			path_to_file = os.path.join(self.path_to_meshes, filename.replace('.vti', ''))
			evtk.hl.imageToVTK(path_to_file, pointData={'density': self._img.copy()})

	def read_ply(self, filename):
		with timethis('Reading PLY file (point cloud)'):
			path_to_file = os.path.join(self.path_to_meshes, filename)
			self._pcd = o3d.io.read_point_cloud(path_to_file)

	def set_point_cloud(self):
		with timethis('Setting point cloud'):
			self._pcd = o3d.geometry.PointCloud()
			self._pcd.points = o3d.utility.Vector3dVector(np.vstack(np.where(self._img == 0)).T)
			self._points = self._pcd.points

	def save_ply(self, filename='output.ply'):
		with timethis('Saving PLY file (point cloud)'):
			path_to_file = os.path.join(self.path_to_meshes, filename)
			o3d.io.write_point_cloud(path_to_file, self._pcd)