from .__init__ import *
import pymesh
import time


'''
	To do:
	[x] Split the simplified mesh on edges with 3+ elements;
	[x] Save the set of points of each mesh;
	[ ] Fit NURBS surface to each set of points.

	References:
	https://github.com/PyMesh/PyMesh/blob/main/python/pymesh/meshutils/separate_mesh.py
'''


class Simplifier(Foo2Pnts):
	def __init__(self, argsdict):
		super(Simplifier, self).__init__(argsdict)

	def read_geometry(self, filename):
		st = time.time()
		self._meshes = pymesh.separate_mesh(pymesh.load_mesh(os.path.join(self.path_to_meshes, filename)))
		self._meshes = [self._meshes] if not isinstance(self._meshes, list) else self._meshes
		et = time.time()
		print('| Reading geometry: {:.3f} seconds'.format(et - st))

	def remove_small_features(self, reduc=1e-3):
		st = time.time()
		meshes = []
		target_area = np.prod(self._shape)**(2/3)*reduc
		for mesh in self._meshes:
			mesh.add_attribute('face_area')
			area = mesh.get_attribute('face_area').sum()
			meshes.append(mesh) if area > target_area else None
		self._meshes = meshes
		et = time.time()
		print('| Removing small features: {:.3f} seconds'.format(et - st))

	def coarse_grid(self, iter=10):
		print('| Coarsening grid...')
		st0 = time.time()
		target_len = np.min(self._shape)*self.coarsen_factor
		meshes_dict = {}
		for k, mesh in enumerate(self._meshes):
			st1 = time.time()
			mesh, _ = pymesh.remove_duplicated_faces(mesh)
			mesh, _ = pymesh.remove_degenerated_triangles(mesh, 100)
			count = 0
			mesh, _ = pymesh.split_long_edges(mesh, target_len)
			num_vertices = mesh.num_vertices
			while True:
				mesh, _ = pymesh.collapse_short_edges(mesh, target_len*1e-3, preserve_feature=True)
				mesh, _ = pymesh.split_long_edges(mesh, target_len*2.5)
				mesh, _ = pymesh.collapse_short_edges(mesh, target_len, preserve_feature=True)
				mesh, _ = pymesh.remove_obtuse_triangles(mesh, 150.0, 100)
				if mesh.num_vertices == num_vertices or mesh.num_vertices == 0: break
				num_vertices = mesh.num_vertices
				count += 1
				if count > iter: break
			mesh = pymesh.resolve_self_intersection(mesh)
			mesh, _ = pymesh.remove_duplicated_faces(mesh)
			mesh, _ = pymesh.remove_obtuse_triangles(mesh, 179.0, 5)
			mesh, _ = pymesh.remove_isolated_vertices(mesh)
			meshes_list = pymesh.separate_mesh(mesh)
			meshes_list = [meshes_list] if not isinstance(meshes_list, list) else meshes_list
			for mesh_ in meshes_list:
				mesh_.add_attribute('face_area')
				meshes_dict[mesh_.get_attribute('face_area').sum()] = mesh_
			et1 = time.time()
			print('| | Coarsening grid, step {}: {:.3f} seconds'.format(k + 1, et1 - st1))
		self._meshes = [meshes_dict[key] for key in sorted(meshes_dict.keys(), reverse=True)]
		et0 = time.time()
		print('| Coarsening grid: {:.3f} seconds'.format(et0 - st0))

	def collapse_outter_vertices(self):
		print('| Collapsing outer vertices')
		st0 = time.time()
		tol = self.coarsen_factor/2
		for k, mesh in enumerate(self._meshes):
			st1 = time.time()
			vertices = mesh.vertices.copy()
			faces = mesh.faces.copy()
			for i in range(3):
				vertices[np.where(vertices[:, i] < self._shape[i]*tol), i] = 0
				vertices[np.where(vertices[:, i] > self._shape[i]*(1 - tol)), i] = self._shape[i]
			mesh = pymesh.form_mesh(vertices, faces)
			mesh = pymesh.resolve_self_intersection(mesh)
			mesh, _ = pymesh.remove_duplicated_faces(mesh)
			mesh, _ = pymesh.remove_obtuse_triangles(mesh, 179.0, 5)
			mesh, _ = pymesh.remove_isolated_vertices(mesh)
			self._meshes[k] = mesh
			et1 = time.time()
			print('| | Collapsing outer vertices, step {}: {:.3f} seconds'.format(k + 1, et1 - st1))
		et0 = time.time()
		print('| Collapsing outer vertices: {:.3f} seconds'.format(et0 - st0))

	def remove_on_boundary(self):
		print('| Removing features on boundary')
		st0 = time.time()
		for k, mesh in enumerate(self._meshes):
			st1 = time.time()
			vertices = mesh.vertices.copy()
			faces = []
			for face in mesh.faces.copy():
				on_boundary = False
				for i in range(3):
					on_boundary = on_boundary or np.all(vertices[face][:, i] == self._shape[i]) or np.all(vertices[face][:, i] == 0)
				faces.append(face) if not on_boundary else None
			faces = np.array(faces).reshape(-1, 3)
			mesh = pymesh.form_mesh(vertices, faces)
			mesh, _ = pymesh.remove_isolated_vertices(mesh)
			self._meshes[k] = mesh
			et1 = time.time()
			print('| | Removing features on boundary, step {}: {:.3f} seconds'.format(k + 1, et1 - st1))
		et0 = time.time()
		print('| Removing features on boundary: {:.3f} seconds'.format(et0 - st0))

	def fix(self):
		st0 = time.time()
		mesh = pymesh.merge_meshes(self._meshes)
		mesh, _ = pymesh.remove_duplicated_faces(mesh)
		mesh, _ = pymesh.remove_obtuse_triangles(mesh, 179.0, 5)
		mesh, _ = pymesh.remove_isolated_vertices(mesh)
		pymesh.is_edge_manifold(mesh)
		pymesh.is_vertex_manifold(mesh)
		mesh = pymesh.cut_to_manifold(mesh)
		self._meshes = pymesh.separate_mesh(mesh)
		meshes = []
		for mesh in self._meshes:
			if mesh.num_vertices > 5:
				meshes.append(mesh)
		self._meshes = meshes
		et0 = time.time()
		print('| Fixing fracture geometry: {:.3f} seconds'.format(et0 - st0))

	def save_geometry(self):
		print('| Saving fracture geometry')
		st0 = time.time()
		[os.remove(os.path.join(self.path_to_meshes, f)) for f in os.listdir(self.path_to_meshes) if os.path.isfile(os.path.join(self.path_to_meshes, f)) and 'INT_FRAC_OP' in f]
		et0 = time.time()
		print('| | Deleting old fracture geometry: {:.3f} seconds'.format(et0 - st0))
		for k, mesh in enumerate(self._meshes):
			pymesh.save_mesh(os.path.join(self.path_to_meshes, 'INT_FRAC_OP{:0>4d}_S.stl'.format(k + 1)), mesh)
			np.savetxt(os.path.join(self.path_to_meshes, 'INT_FRAC_OP{:0>4d}_S.txt'.format(k + 1)), mesh.vertices.copy(), delimiter=',')
		et1 = time.time()
		print('| | Saving new fracture geometry: {:.3f} seconds'.format(et1 - et0))
		print('| Saving fracture geometry: {:.3f} seconds'.format(et1 - st0))