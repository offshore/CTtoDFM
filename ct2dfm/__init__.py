import numpy as np
import os
import time


'''
	To-do:
	[x] Fix orientation of surfaces (scikit-spatial)
	[x] Calculate average aperture/permeability for each element (scikit-spatial/NumPy)
	[x] Improve element aperture/permeability calculation by including missing points (scikit-spatial/NumPy)
	[x] Calculate average density for each element (scikit-spatial/NumPy)
	[x] Populate porosity/permeability based on density (NumPy)
	[ ] Set apertures smaller than 1 pixel according to linear relationship with CT-density data (NumPy)
	[ ] Break fine mesh into non-contiguous regions (PyMesh)
	[ ] Coarsen each non-contiguous mesh to remove noise (PyMesh)
	[ ] Save the points of each non-contiguous fine mesh corresponding to the remaining non-contiguous coarse mesh (PyMesh)
	[ ] Fit NURBS to each set of points (Rhino)
	[ ] Intersect and trim NURBS (Rhino)
	[ ] Generate quad-hexahedra mesh (ANSYS/ansys2meshio)
'''


class Dict2ClassVar(object):
	def __init__(self, argsdict):
		super(Dict2ClassVar, self).__init__()    
		for key in argsdict.keys():
			setattr(self, key, argsdict[key])
		if not os.path.isdir(self.path_to_images):
			os.makedirs(self.path_to_images, exist_ok=True)
		if not os.path.isdir(self.path_to_meshes):
			os.makedirs(self.path_to_meshes, exist_ok=True)
		if not os.path.isdir('.cache'):
			os.makedirs('.cache', exist_ok=True)


class Foo2Pnts(Dict2ClassVar):
	def __init__(self, argsdict):
		super(Foo2Pnts, self).__init__(argsdict)
		self._img = False
		self._aperture = []

	def __str__(self):
		return str(self._img)

	def get_3D_image(self):
		return self._img

	def set_3D_image(self, img):
		self._img = img
		self._shape = img.shape

	def get_shape(self):
		return self._shape

	def set_shape(self, shape):
		self._shape = shape

	def save_shape(self, filename='shape.txt'):
		st = time.time()
		np.savetxt(os.path.join(self.path_to_meshes, filename), self._shape, delimiter=',')
		et = time.time()
		print('Saving shape: {:.3f} seconds'.format(et - st))

	def read_shape(self, filename='shape.txt'):
		st = time.time()
		self._shape = np.loadtxt(os.path.join(self.path_to_meshes, filename), delimiter=',').astype(int)
		et = time.time()
		print('Reading shape: {:.3f} seconds'.format(et - st))

	def save_points(self, filename='points.txt'):
		st = time.time()
		np.savetxt(os.path.join(self.path_to_meshes, filename), self._points, delimiter=',')
		et = time.time()
		print('Saving points: {:.3f} seconds'.format(et - st))
	
	def save_point_data(self, filename='point_data.txt'):
		st = time.time()
		np.savetxt(os.path.join(self.path_to_meshes, filename), self._aperture, delimiter=',')
		et = time.time()
		print('Saving point data: {:.3f} seconds'.format(et - st))

	def read_points(self, filename):
		st = time.time()
		self._points = np.loadtxt(fname=os.path.join(self.path_to_meshes, filename), delimiter=',')
		et = time.time()
		print('Reading points: {:.3f} seconds'.format(et - st))
		
	def read_point_data(self, filename):
		st = time.time()
		self._aperture = np.loadtxt(fname=os.path.join(self.path_to_meshes, filename), delimiter=',')
		et = time.time()
		print('Reading point data: {:.3f} seconds'.format(et - st))
