import argparse
from parameters import *


parser = argparse.ArgumentParser()
parser.add_argument('--action', required=True, type=str)
parser.add_argument('--sample', required=True, type=str)
args = parser.parse_args()
assert args.sample in params.keys(), 'Sample not in the parameters list.'
if __name__ == '__main__':
	if args.action == 'crop CT scans':
		print('Cropping CT scans for sample {}...'.format(args.sample))
		from ct2dfm.ct2pnts import CT2Pnts
		ct2pnts = CT2Pnts(params[args.sample])
		ct2pnts.read_ct_scans(0)
		ct2pnts.crop()
		ct2pnts.save_vtk(filename='cropped.vti')
		ct2pnts.save_shape()
	elif args.action == 'threshold':
		print('Thresholding sample {}...'.format(args.sample))
		from ct2dfm.ct2pnts import CT2Pnts
		ct2pnts = CT2Pnts(params[args.sample])
		ct2pnts.read_vtk(filename='cropped.vti')
		ct2pnts.threshold()
		ct2pnts.save_vtk(filename='segmented.vti')
	elif args.action == 'skeletonize':
		print('Skeletonizing sample {}...'.format(args.sample))
		from ct2dfm.skeletonization import Skeletonizer
		skel = Skeletonizer(params[args.sample])
		skel.read_vtk(filename='segmented.vti')
		skel.skeletonize()
		skel.save_ply(filename='skeleton_cloud.ply')
		del skel
		from ct2dfm.skeletonization import Clusterizer
		clust = Clusterizer(params[args.sample])
		clust.read_ply(filename='skeleton_cloud.ply')
		clust.dbscan()
		clust.save_ply(filename='skeleton_dbscan.ply')
		clust.save_points(filename='skeleton_points.txt')
		clust.save_point_data(filename='skeleton_point_data.txt')
		del clust
		from ct2dfm.pnts2geom import Pnts2Geom
		pnts2geom = Pnts2Geom(params[args.sample])
		pnts2geom.read_shape()
		pnts2geom.set_rhino_path(rhino_path)
		pnts2geom.generate_fracture_geometry(input_file='skeleton_points.txt', output_file='skeleton_geometry.stl')
	elif args.action == 'generate geometry':
		print('Generating geometry for sample {}...'.format(args.sample))
		from ct2dfm.pnts2geom import Pnts2Geom
		pnts2geom = Pnts2Geom(params[args.sample])
		pnts2geom.read_shape()
		pnts2geom.set_rhino_path(rhino_path)
		pnts2geom.simplify_fracture_geometry(args.sample)
		pnts2geom.generate_model_geometry()
	elif args.action == 'simplify geometry': # this action is called inside a docker
		from ct2dfm.simplification import Simplifier
		simp = Simplifier(params[args.sample])
		simp.read_shape()
		simp.read_geometry(filename='skeleton_geometry.stl')
		simp.remove_small_features()
		simp.coarse_grid()
		simp.collapse_outter_vertices()
		simp.remove_on_boundary()
		simp.fix()
		simp.remove_small_features()
		simp.save_geometry()
	elif args.action == 'generate mesh':
		print('Meshing sample {}...'.format(args.sample))
		from ct2dfm.geom2mesh import Geom2Mesh
		geom2mesh = Geom2Mesh(params[args.sample])
		geom2mesh.read_shape()
		geom2mesh.set_frac_list()
		geom2mesh.set_ansys_path(ansys_path)
		geom2mesh.convert_to_tin()
		geom2mesh.generate_mesh()
		del geom2mesh
		from ct2dfm.geom2mesh import MeshFix
		meshfix = MeshFix(params[args.sample])
		meshfix.set_ansys_path(ansys_path)
		meshfix.fix_boundary()
		meshfix.fix_mesh_orientation()
		meshfix.read_density(filename='cropped.vti')
		meshfix.interpolate_density()
		meshfix.calculate_porosity()
		meshfix.populate_permeability()
		meshfix.generate_fracture_mesh()
		meshfix.read_skeleton_points(filename='skeleton_points.txt')
		meshfix.read_skeleton_aperture(filename='skeleton_point_data.txt')
		meshfix.interpolate_aperture()
		meshfix.save_as(format='xdmf', save_frac=True)
		meshfix.convert_to_csp()
	else:
		raise ValueError('Invalid action')